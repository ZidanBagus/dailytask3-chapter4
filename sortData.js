// import data

const data = require('./datane');

const errorHandler = () => {
  const err = {
    message: "data tidak ada / tidak ditemukan"
  };
  return err;
};

const dataSiji = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].age < 30 && data[i].favoriteFruit === "banana") {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const dataLoro = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    if ((data[i].gender === "female" || data[i].company === "FSW4") && data[i].age > 30) {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const dataTelu = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    if (data[i].eyeColor === "blue" && (data[i].age >= 35 && data[i].age <= 40) && data[i].favoriteFruit === "apple") {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const dataPapat = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    if ((data[i].company === "Pelangi" || data[i].company === "Intel") && data[i].eyeColor === "green") {
      filteredData.push(data[i]);
    }
  }

  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const dataLima = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    let year = (new Date(data[i].registered)).getFullYear();
    if (year < 2016 && data[i].isActive) {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
}

const goletData = (referenceWords) => {
  let referenceWord = referenceWords.toLowerCase();
  const datas = data.filter(
    (datas) =>
      datas.eyeColor.toLowerCase() === referenceWord || datas.favoriteFruit.toLowerCase() === referenceWord || datas.company.toLowerCase() === referenceWord || datas.age.toString() === referenceWord || datas.gender.toLowerCase() === referenceWord
  );
  return datas;
};


  


module.exports = { dataSiji, dataLoro, dataTelu, dataPapat, dataLima, goletData };
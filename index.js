const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const  port =  process.env.port;
const datane = require('./datane');
const app = express();

// import data
const { dataSiji, dataLoro, dataTelu, dataPapat, dataLima, goletData} = require('./sortData');


//static file untuk render file css dan gambar
app.use(express.static('public'))

// set view engine menggunakan EJS 
app.set('view engine', 'ejs');

///fetch the data from request  
app.use(bodyParser.urlencoded({ extended: false }));



app.get('/', (req, res) => {
  res.render('index')
});
app.post('/datane', (req, res) => {
  const referenceWord = goletData(req.body.search);
  res.render('Contact', {
    Output: referenceWord,
    note: `pencarian berdasarkan = <b>${req.body.search}</b>`,
  });
});
app.get('/datane', (req, res) => {
  res.render('Contact', {
    Output: datane,
    note: 'Tampilkan semua data'
  });
});
app.get('/data1', (req, res) => {
  res.render('Contact', {
    Output: dataSiji(),
    note: 'under 30 years old and favorite is banana'
  });
});
app.get('/data2', (req, res) => {
  res.render('Contact', {
    Output: dataLoro(),
    note: 'female gender or FSW4 company and over 30 years old'
  });
});
app.get('/data3', (req, res) => {
  res.render('Contact', {
    Output: dataTelu(),
    note: 'Blue eye color and age between 35 to 40, and  favorite is apples'
  });
});

app.get('/data4', (req, res) => {
  res.render('Contact', {
    Output: dataPapat(),
    note: 'Rainbow Company or Intel, and green eye color'
  });
});
app.get('/data5', (req, res) => {
  res.render('Contact', {
    Output: dataLima(),
    note: 'Registered under 2016 and still active(true)'
  });
});
app.get('/about', (req, res) => {
  res.render('about')
});

app.use('/', (req, res) => {
  res.status(404)
  res.render('404')

})

app.listen(port, () => {
  console.log(`Server wis mlayu on port ${port}`)
})